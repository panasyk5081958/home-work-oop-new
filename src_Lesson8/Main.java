import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.io.BufferedReader;
import java.io.FileReader;

public class Main {
    public static void main(String[] args) {
        Human human1 = new Human("Oleg", "Vovk", Gender.MASCULINE);
        Student student1 = new Student(human1.getName(), human1.getLastName(), human1.getGender(), 123, "A");

        Human human2 = new Human("Vadim", "Kovalenko", Gender.MASCULINE);
        Student student2 = new Student(human2.getName(), human2.getLastName(), human2.getGender(), 456, "A");

        Human human3 = new Human("Artur", "Shevchuk", Gender.MASCULINE);
        Student student3 = new Student(human3.getName(), human3.getLastName(), human3.getGender(), 789, "B");

        Student[] students = new Student[] {student1, student2, student3};
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i]);
        }
        Arrays.sort(students, Comparator.nullsFirst(new StudentLastNameComparator()));
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i]);
        }
        Group grFirst = new Group();
        grFirst.setGroupName("A");

        GetStudentFromKeyboard studentNew = new GetStudentFromKeyboard ();
        System.out.println(studentNew);

//        try {
//            grFirst.addStudent(student1);
 //       } catch (GroupOverflowException e) {
 //           System.out.println("Group is full");
 //       }
 //       System.out.println(grFirst);

        Human human4 = new Human("Olga", "Petrova", Gender.FEMININE);
        Student student4 = new Student(human4.getName(), human4.getLastName(), human4.getGender(), 910, "A");

        Human human5 = new Human("Oksana", "Petrova", Gender.FEMININE);
        Student student5 = new Student(human5.getName(), human5.getLastName(), human5.getGender(), 555, "B");

        System.out.println(human4.equals(human5));
        System.out.println(human4.hashCode());
        System.out.println(human5.hashCode());
        System.out.println(student4.equals(student5));
        System.out.println(student4.hashCode());
        System.out.println(student5.hashCode());

        Human human6 = new Human("Oksana", "Petrova", Gender.FEMININE);
        Student student6 = new Student(human6.getName(), human6.getLastName(), human6.getGender(), 555, "B");

        System.out.println(human5.equals(human6));
        System.out.println(human6.hashCode());
        System.out.println(student6.equals(student5));
        System.out.println(student6.hashCode());
    }
}


