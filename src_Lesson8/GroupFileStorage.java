import java.io.*;

public class GroupFileStorage {
    public void saveGroupToCSV(Group gr) {
        try {
            File fileCsv = new File("Group.csv");
            if (!fileCsv.exists()) {
                fileCsv.createNewFile();
            }
            String fileContent = "";
            Student[] student = gr.getStudents();

            PrintWriter pw = new PrintWriter(fileCsv);
            for (int i = 0; i < gr.getStudents().length; i++) {
                if (gr.getStudents()[i] != null) {
                    fileContent = student[i].getName() + "" + student[i].getLastName() + "" + student[i].gender + "" + student[i].getId() + "" + gr.getGroupName();
                } else {
                    pw.close();
                }
            }
        } catch (IOException e) {
            System.out.println("Error " + e);
        }
    }
    public Group loadGroupFromCSV(File file) throws GroupOverflowException {

        Group group = new Group();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(file));
            String[] result;
            String line;

            for ( ; ; ) {
                line = br.readLine();
                if (line == null) {
                    break;
                }
                result = line.split(" ");
                for (int i = 0; i < result.length; i++) {
                    Student student = new Student(result[0], result[1], Gender.valueOf(result[2]), Integer.valueOf(result[3]), result[4]);
                    group.addStudent(student);
                }
            }
        } catch (IOException e) {
            System.out.println("Error " + e);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                System.out.println("Error " + e);
            }
        }
        return group;
    }
    public File findFileByGroupName(String groupName, File workFolder) {
        File[] files = workFolder.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].getName().equals(groupName)) {
                return files[i];
            }
        }
        return null;
    }
}
