import java.util.Scanner;

public class GetStudentFromKeyboard {
    Scanner scanner = new Scanner(System.in);

    public void CreateStudent() {

        System.out.println("Enter the name");
        String name = scanner.nextLine();

        System.out.println("Enter the last name");
        String lastName = scanner.nextLine();

        System.out.println("Enter the age");
        int age = scanner.nextInt();

        scanner.nextLine();

        System.out.println("Enter the gender: FEMININE or MASCULINE");
        Gender gender = Gender.valueOf(scanner.nextLine());

        System.out.println("Enter the group name");
        String group = scanner.nextLine();

        Student studentNew = new Student(name, lastName, gender, age, group);
        return;
    }
}
