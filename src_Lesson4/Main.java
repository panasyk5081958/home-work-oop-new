import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Human human1 = new Human("Oleg", "Vovk", Gender.MASCULINE);
        Student student1 = new Student(human1.getName(), human1.getLastName(), human1.getGender(), 123, "A");

        Human human2 = new Human("Vadim", "Kovalenko", Gender.MASCULINE);
        Student student2 = new Student(human2.getName(), human2.getLastName(), human2.getGender(), 456, "A");

        Human human3 = new Human("Artur", "Shevchuk", Gender.MASCULINE);
        Student student3 = new Student(human3.getName(), human3.getLastName(), human3.getGender(), 789, "B");

        Student[] students = new Student[] {student1, student2, student3};
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i]);
        }
        Arrays.sort(students, Comparator.nullsFirst(new StudentLastNameComparator()));
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i]);
        }
        Group grFirst = new Group();
        grFirst.setGroupName("A");

        GetStudentFromKeyboard studentNew = new GetStudentFromKeyboard ();
        System.out.println(studentNew);

        try {
            grFirst.addStudent(student1);
        } catch (GroupOverflowException e) {
            System.out.println("Group is full");
        }
        System.out.println(grFirst);


    }

}


