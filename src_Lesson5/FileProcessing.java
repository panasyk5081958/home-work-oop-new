import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileProcessing {
    public static long copyFile(File fileIn, File outOut) throws IOException {
        try (InputStream is = new FileInputStream(fileIn);
            OutputStream os = new FileOutputStream(outOut)) {
            return is.transferTo(os);
        }

    }

    public static int copyFileByExtension (File folderIn, File folderOut) throws IOException {
        File [] files = folderIn.listFiles();
        int copyFile = 0;
        for (int i = 0; i < files.length; i++) {

            boolean b = files[i].getName().endsWith(".docx");
            if (files[i].isFile() && b == true) {
                File fileOut = new File(folderOut, files[i].getName());
                copyFile (files[i], fileOut);
                copyFile += 1;
            }
        }
        return copyFile;
    }
}
